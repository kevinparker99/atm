﻿namespace CDI_ATM
{
    partial class Supervisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Funds = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Notification = new System.Windows.Forms.TextBox();
            this.KeyPad = new System.Windows.Forms.GroupBox();
            this.Add = new System.Windows.Forms.Button();
            this.Amount = new System.Windows.Forms.TextBox();
            this.Transaction = new System.Windows.Forms.GroupBox();
            this.Shutdown = new System.Windows.Forms.RadioButton();
            this.Print = new System.Windows.Forms.RadioButton();
            this.Refill = new System.Windows.Forms.RadioButton();
            this.Interest = new System.Windows.Forms.RadioButton();
            this.Submit = new System.Windows.Forms.Button();
            this.Close_button = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.Action = new System.Windows.Forms.Label();
            this.labelAmount = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.KeyPad.SuspendLayout();
            this.Transaction.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(-1, 319);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 49;
            this.label1.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Funds);
            this.groupBox2.Location = new System.Drawing.Point(223, 158);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 71);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Funds in ATM:";
            // 
            // Funds
            // 
            this.Funds.Location = new System.Drawing.Point(6, 20);
            this.Funds.Name = "Funds";
            this.Funds.ReadOnly = true;
            this.Funds.Size = new System.Drawing.Size(188, 20);
            this.Funds.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Notification);
            this.groupBox3.Location = new System.Drawing.Point(12, 158);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 65);
            this.groupBox3.TabIndex = 38;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Notification";
            // 
            // Notification
            // 
            this.Notification.Location = new System.Drawing.Point(6, 20);
            this.Notification.Multiline = true;
            this.Notification.Name = "Notification";
            this.Notification.ReadOnly = true;
            this.Notification.Size = new System.Drawing.Size(188, 39);
            this.Notification.TabIndex = 0;
            // 
            // KeyPad
            // 
            this.KeyPad.Controls.Add(this.Add);
            this.KeyPad.Controls.Add(this.Amount);
            this.KeyPad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPad.Location = new System.Drawing.Point(12, 10);
            this.KeyPad.Name = "KeyPad";
            this.KeyPad.Size = new System.Drawing.Size(175, 129);
            this.KeyPad.TabIndex = 36;
            this.KeyPad.TabStop = false;
            this.KeyPad.Text = "Refill Amount";
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(39, 43);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(99, 23);
            this.Add.TabIndex = 13;
            this.Add.Text = "Add $5000";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Amount
            // 
            this.Amount.Location = new System.Drawing.Point(16, 88);
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.Size = new System.Drawing.Size(144, 23);
            this.Amount.TabIndex = 12;
            this.Amount.Text = "0";
            // 
            // Transaction
            // 
            this.Transaction.Controls.Add(this.Shutdown);
            this.Transaction.Controls.Add(this.Print);
            this.Transaction.Controls.Add(this.Refill);
            this.Transaction.Controls.Add(this.Interest);
            this.Transaction.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Transaction.Location = new System.Drawing.Point(223, 12);
            this.Transaction.Name = "Transaction";
            this.Transaction.Size = new System.Drawing.Size(200, 111);
            this.Transaction.TabIndex = 35;
            this.Transaction.TabStop = false;
            this.Transaction.Text = "Select Action:";
            // 
            // Shutdown
            // 
            this.Shutdown.AutoSize = true;
            this.Shutdown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Shutdown.Location = new System.Drawing.Point(6, 88);
            this.Shutdown.Name = "Shutdown";
            this.Shutdown.Size = new System.Drawing.Size(99, 17);
            this.Shutdown.TabIndex = 3;
            this.Shutdown.TabStop = true;
            this.Shutdown.Text = "Shutdown ATM";
            this.Shutdown.UseVisualStyleBackColor = true;
            this.Shutdown.CheckedChanged += new System.EventHandler(this.Shutdown_CheckedChanged);
            // 
            // Print
            // 
            this.Print.AutoSize = true;
            this.Print.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Print.Location = new System.Drawing.Point(6, 66);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(127, 17);
            this.Print.TabIndex = 2;
            this.Print.TabStop = true;
            this.Print.Text = "Print account Service";
            this.Print.UseVisualStyleBackColor = true;
            this.Print.CheckedChanged += new System.EventHandler(this.Print_CheckedChanged);
            // 
            // Refill
            // 
            this.Refill.AutoSize = true;
            this.Refill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Refill.Location = new System.Drawing.Point(6, 43);
            this.Refill.Name = "Refill";
            this.Refill.Size = new System.Drawing.Size(72, 17);
            this.Refill.TabIndex = 1;
            this.Refill.TabStop = true;
            this.Refill.Text = "Refil ATM";
            this.Refill.UseVisualStyleBackColor = true;
            this.Refill.CheckedChanged += new System.EventHandler(this.Refill_CheckedChanged);
            // 
            // Interest
            // 
            this.Interest.AutoSize = true;
            this.Interest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Interest.Location = new System.Drawing.Point(6, 20);
            this.Interest.Name = "Interest";
            this.Interest.Size = new System.Drawing.Size(81, 17);
            this.Interest.TabIndex = 0;
            this.Interest.TabStop = true;
            this.Interest.Text = "Pay Interest";
            this.Interest.UseVisualStyleBackColor = true;
            this.Interest.CheckedChanged += new System.EventHandler(this.Interest_CheckedChanged);
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(185, 243);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(75, 23);
            this.Submit.TabIndex = 34;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // Close
            // 
            this.Close_button.Location = new System.Drawing.Point(348, 243);
            this.Close_button.Name = "Close";
            this.Close_button.Size = new System.Drawing.Size(75, 23);
            this.Close_button.TabIndex = 33;
            this.Close_button.Text = "Close";
            this.Close_button.UseVisualStyleBackColor = true;
            this.Close_button.Click += new System.EventHandler(this.Close_Click);
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(267, 243);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(75, 23);
            this.Clear.TabIndex = 32;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Action
            // 
            this.Action.AutoSize = true;
            this.Action.ForeColor = System.Drawing.Color.Red;
            this.Action.Location = new System.Drawing.Point(229, 126);
            this.Action.Name = "Action";
            this.Action.Size = new System.Drawing.Size(132, 13);
            this.Action.TabIndex = 50;
            this.Action.Text = "You must select an action.";
            this.Action.Visible = false;
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = true;
            this.labelAmount.ForeColor = System.Drawing.Color.Red;
            this.labelAmount.Location = new System.Drawing.Point(15, 145);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(324, 13);
            this.labelAmount.TabIndex = 51;
            this.labelAmount.Text = "Amount cannot be zero or make the ATM have more than $20,000.";
            this.labelAmount.Visible = false;
            // 
            // Supervisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 278);
            this.Controls.Add(this.labelAmount);
            this.Controls.Add(this.Action);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.KeyPad);
            this.Controls.Add(this.Transaction);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.Close_button);
            this.Controls.Add(this.Clear);
            this.Name = "Supervisor";
            this.Text = "Supervisor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Supervisor_FormClosing);
            this.Load += new System.EventHandler(this.Supervisor_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.KeyPad.ResumeLayout(false);
            this.KeyPad.PerformLayout();
            this.Transaction.ResumeLayout(false);
            this.Transaction.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox Funds;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox Notification;
        private System.Windows.Forms.GroupBox KeyPad;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.TextBox Amount;
        private System.Windows.Forms.GroupBox Transaction;
        private System.Windows.Forms.RadioButton Shutdown;
        private System.Windows.Forms.RadioButton Print;
        private System.Windows.Forms.RadioButton Refill;
        private System.Windows.Forms.RadioButton Interest;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.Button Close_button;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Label Action;
        private System.Windows.Forms.Label labelAmount;
    }
}