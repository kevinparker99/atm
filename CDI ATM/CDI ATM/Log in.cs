﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATMClassLibrary;

namespace CDI_ATM
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            new ATMManager().Start();
        }
        static public bool exit = false;
        int korbenInt = 0;
        int jerryInt = 0;
        int ericInt = 0;
        int carlosInt = 0;
        int eltonInt = 0;
        
        void Korben()
        {
            korbenInt++;
            if (!(korbenInt<3))
            {
                TryAgain.Visible = true;
                new ATMManager().RemoveCustomer(name.Text);
            }
        }

        void Jerry()
        {
            jerryInt++;
            if (!(jerryInt < 3))
            {
                TryAgain.Visible = true;
                new ATMManager().RemoveCustomer(name.Text);
            }
        }

        void Eric()
        {
            ericInt++;
            if (!(ericInt < 3))
            {
                TryAgain.Visible = true;
                new ATMManager().RemoveCustomer(name.Text);
            }
        }

        void Carlos()
        {
            carlosInt++;
            if (!(carlosInt < 3))
            {
                TryAgain.Visible = true;
                new ATMManager().RemoveCustomer(name.Text);
            }
        }

        void Elton()
        {
            eltonInt++;
            if (eltonInt >= 3)
            {
                TryAgain.Visible = true;
                new ATMManager().RemoveCustomer(name.Text);
            }
        }

        private void Erase_Click(object sender, EventArgs e)
        {
            name.Text = "";
            PIN.Text = "";
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            Wrong.Visible = false;
            TryAgain.Visible = false;
            int check = new ATMManager().ValidateUser(name.Text, PIN.Text);
            switch (check)
            {
                case 0:
                    Empty.Visible = true;
                    break;
                case 1:
                    Wrong.Visible = true;
                    break;
                case 2:
                    string[] fName = name.Text.Split(' ');
                    switch (fName[0])
                    {
                        case "Korben":
                            Korben();
                            break;
                        case "Jerry":
                            Jerry();
                            break;
                        case "Eric":
                            Eric();
                            break;
                        case "Carlos":
                            Carlos();
                            break;
                        case "Elton":
                            Elton();
                            break;
                    }
                    Wrong.Visible = true;
                    break;
                case 3:
                    Main main = new Main(PIN.Text)
                    {
                        Visible = true
                    };
                    main.Activate();
                    this.Visible = false;
                    break;
                case 4:
                    Supervisor supervisor = new Supervisor
                    {
                        Visible = true
                    };
                    supervisor.Activate();
                    this.Visible = true;
                    break;
            }
            
        }

        

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                Erase_Click(sender, e);
                this.Visible = true;
                e.Cancel = true;
                return;
            }
            else
            {
                new ATMManager().Closing();
                Application.Exit();
            }
                
        }
    }
}
