﻿namespace CDI_ATM
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.PIN = new System.Windows.Forms.TextBox();
            this.Submit = new System.Windows.Forms.Button();
            this.Erase = new System.Windows.Forms.Button();
            this.Wrong = new System.Windows.Forms.Label();
            this.Empty = new System.Windows.Forms.Label();
            this.TryAgain = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(115, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Log into your account:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(114, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(117, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "PIN:";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(170, 131);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(100, 20);
            this.name.TabIndex = 1;
            // 
            // PIN
            // 
            this.PIN.Location = new System.Drawing.Point(170, 190);
            this.PIN.Name = "PIN";
            this.PIN.PasswordChar = '*';
            this.PIN.Size = new System.Drawing.Size(100, 20);
            this.PIN.TabIndex = 3;
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(120, 281);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(75, 23);
            this.Submit.TabIndex = 4;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // Erase
            // 
            this.Erase.Location = new System.Drawing.Point(239, 282);
            this.Erase.Name = "Erase";
            this.Erase.Size = new System.Drawing.Size(75, 22);
            this.Erase.TabIndex = 5;
            this.Erase.Text = "Erase";
            this.Erase.UseVisualStyleBackColor = true;
            this.Erase.Click += new System.EventHandler(this.Erase_Click);
            // 
            // Wrong
            // 
            this.Wrong.AutoSize = true;
            this.Wrong.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Wrong.ForeColor = System.Drawing.Color.Red;
            this.Wrong.Location = new System.Drawing.Point(120, 88);
            this.Wrong.Name = "Wrong";
            this.Wrong.Size = new System.Drawing.Size(172, 17);
            this.Wrong.TabIndex = 6;
            this.Wrong.Text = "Name and/or PIN is wrong";
            this.Wrong.Visible = false;
            // 
            // Empty
            // 
            this.Empty.AutoSize = true;
            this.Empty.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Empty.ForeColor = System.Drawing.Color.Red;
            this.Empty.Location = new System.Drawing.Point(120, 88);
            this.Empty.Name = "Empty";
            this.Empty.Size = new System.Drawing.Size(206, 17);
            this.Empty.TabIndex = 7;
            this.Empty.Text = "Name and/or PIN isn\'t filled out.";
            this.Empty.Visible = false;
            // 
            // TryAgain
            // 
            this.TryAgain.AutoSize = true;
            this.TryAgain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TryAgain.ForeColor = System.Drawing.Color.Red;
            this.TryAgain.Location = new System.Drawing.Point(65, 231);
            this.TryAgain.Name = "TryAgain";
            this.TryAgain.Size = new System.Drawing.Size(310, 17);
            this.TryAgain.TabIndex = 8;
            this.TryAgain.Text = "You got your PIN wrong 3 times. Try again later.";
            this.TryAgain.Visible = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 347);
            this.Controls.Add(this.TryAgain);
            this.Controls.Add(this.Empty);
            this.Controls.Add(this.Wrong);
            this.Controls.Add(this.Erase);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.PIN);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Login";
            this.Text = "Log in";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Login_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox PIN;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.Button Erase;
        private System.Windows.Forms.Label Wrong;
        private System.Windows.Forms.Label Empty;
        private System.Windows.Forms.Label TryAgain;
    }
}

