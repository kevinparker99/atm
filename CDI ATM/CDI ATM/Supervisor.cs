﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CDI_ATM
{
    public partial class Supervisor : Form
    {
        public Supervisor()
        {
            InitializeComponent();
        }

        private void Supervisor_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Supervisor_Load(object sender, EventArgs e)
        {
            Funds.Text = $"{ATMClassLibrary.ATMManager.bank.AccountBalance}";
        }

        //
        // Select Action
        //
        private void Interest_CheckedChanged(object sender, EventArgs e)
        {
            if (Interest.Checked)
                Notification.Text = $"You will pay {ATMClassLibrary.Saving.InterestRate}% to all saving accounts.";
        }

        private void Refill_CheckedChanged(object sender, EventArgs e)
        {
            if (Refill.Checked)
                Notification.Text = $"You need to refill the with a multiple ${ATMClassLibrary.Bank.RefillAmount} and make sure the amount in the ATM doesn't excessed ${ATMClassLibrary.Bank.MaxTopUp}.";
        }

        private void Shutdown_CheckedChanged(object sender, EventArgs e)
        {
            if (Shutdown.Checked)
                Notification.Text = "This will shut down the ATM completely if their is a problem that is needed to be fixed in the on a later date.";
        }

        private void Print_CheckedChanged(object sender, EventArgs e)
        {
            if (Print.Checked)
                Notification.Text = "this will pull up a message box with all the accounts printed on it.";
        }


        //
        // Submit button
        //
        private void Submit_Click(object sender, EventArgs e)
        {
            Action.Visible = false;
            labelAmount.Visible = false;

            if(!Refill.Checked && !Interest.Checked && !Print.Checked && !Shutdown.Checked)
            {
                Action.Visible = true;
                return;
            }
            if (Refill.Checked)
            {
                if((ATMClassLibrary.ATMManager.bank.AccountBalance + Convert.ToDouble(Amount.Text))== ATMClassLibrary.Bank.MaxTopUp)
                {
                    labelAmount.Visible = true;
                    return;
                }
                ATMClassLibrary.ATMManager.bank.RefillATM(Convert.ToDouble(Amount.Text));
                Refill_CheckedChanged(sender, e);
            }
            if(Interest.Checked)
            {
                new ATMClassLibrary.ATMManager().PayInterest();
            }
            if (Print.Checked)
            {
                string i = new ATMClassLibrary.ATMManager().Print();
                MessageBox.Show(i,"Printed accounts");
            }

            if(Shutdown.Checked)
            {
                Login.exit = true;
                Application.Exit();
            }
        }

        

        //
        // Other Buttons
        //

        private void Clear_Click(object sender, EventArgs e)
        {
            Amount.Text = "";
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Amount.Text) >= ATMClassLibrary.Bank.MaxTopUp)
            {
                Amount.Text = "0";
                return;
            }
            Amount.Text = $"{Convert.ToInt32(Amount.Text) + ATMClassLibrary.Bank.RefillAmount}";
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
            this.Close();
        }
    }
}
