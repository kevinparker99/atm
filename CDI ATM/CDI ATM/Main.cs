﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ATMClassLibrary;
using System.Threading;

namespace CDI_ATM
{
    public partial class Main : Form
    {
        readonly string customer;
        Chequing cheque;
        Saving save;


        public Main(string PIN)
        {
            InitializeComponent();
            customer = PIN;
            Accounts();
        }

        

        void Accounts()
        {
            (cheque, save) = new ATMManager().GetAccounts(customer);
        }
        private void Exit_Click(object sender, EventArgs e)
        {

            Application.Exit();
            Close();
        }


        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            Value.Visible = false;
            labelBill.Visible = false;
            labelTransfer.Visible = false;
            labelWithdrawal.Visible = false;
            labelWithdrawalChequing.Visible = false;
            labelAccount.Visible = false;
            labelTransaction.Visible = false;
            labelBillSaving.Visible = false;
            Amount.Text = "";
        }


        //
        //
        // KeyPad
        //
        //

        private void button1_Click(object sender, EventArgs e)
        {
            Amount.Text += '1';
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Amount.Text += '2';
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Amount.Text += '3';
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Amount.Text += '4';
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Amount.Text += '5';
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Amount.Text += '6';
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Amount.Text += '7';
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Amount.Text += '8';
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Amount.Text += '9';
        }

        private void button0_Click(object sender, EventArgs e)
        {
            Amount.Text += '0';
        }

        private void Period_Click(object sender, EventArgs e)
        {
            Amount.Text += '.';
        }





        //
        //Submit Button
        // 
        private void Submit_Click(object sender, EventArgs e)
        {
            labelNoMoney.Visible = false;
            Value.Visible = false;
            labelBill.Visible = false;
            labelTransfer.Visible = false;
            labelWithdrawal.Visible = false;
            labelWithdrawalChequing.Visible  = false;
            labelAccount.Visible = false;
            labelTransaction.Visible = false;
            labelBillSaving.Visible = false;

            if (!Chequing.Checked && !Saving.Checked)
                labelAccount.Visible = true;
            if (!Transfer.Checked && !Withdrawal.Checked && !Pay_Bill.Checked && !Deposit.Checked)
            {
                labelTransaction.Visible = true;
            }
            if (!Test.AmountTest(Amount.Text))
            {
                Value.Visible = true;
            }
            if (labelTransaction.Visible || labelAccount.Visible || Value.Visible)
                return;
            double amount = Convert.ToDouble(Amount.Text);
            if (Deposit.Checked)
            {
                if (Chequing.Checked)
                {
                    cheque.Deposit(amount);
                    Notification.Text = "Deposit sucessful.";
                    Chequing_CheckedChanged(sender, e);
                    return;
                }
                if (Saving.Checked)
                {
                    save.Deposit(amount);
                    Notification.Text = "Deposit sucessful.";
                    Saving_CheckedChanged(sender, e);
                    return;
                }    
            }
            if (Withdrawal.Checked)
            {
                if (Chequing.Checked)
                {
                    if (ATMManager.bank.AccountBalance == 0)
                    {
                        labelNoMoney.Visible = true;
                        return;
                    }
                    if (amount% 10 != 0)
                    {
                        labelWithdrawal.Visible = true;
                        
                    }
                    if (amount > 1000)
                        labelWithdrawalChequing.Visible = true;
                    if (labelWithdrawalChequing.Visible || labelWithdrawal.Visible)
                    {
                        Notification.Text = "Withdrawal unsucessful.";
                        return;
                    }
                    bool check = ATMManager.bank.Withdraw(amount);
                    if (!check)
                    {
                        Amount.Text = $"{ATMManager.bank.AccountBalance}";
                        MessageBox.Show($"This ATM doesnt have enough money to complete your request.\n${Amount.Text} is left.", "ATM doesn't have enough money", MessageBoxButtons.OK);
                        return;
                    }
                    check = cheque.Withdraw(amount);
                    if (check)
                    {
                        
                        Notification.Text = "Withdrawal sucessful.";
                        Chequing_CheckedChanged(sender, e);
                        ATMClassLibrary.ATMManager.dailyBalance.UpdateDailyBalance(Convert.ToInt32(-amount));
                        return;
                    }
                    else
                    {
                        Notification.Text = "Withdrawal unsucessful. Not enough funds.";
                        return;
                    }
                }
                if (Saving.Checked)
                {

                    bool check = ATMManager.bank.Withdraw(amount);
                    if (!check)
                    {
                        Amount.Text = $"{ATMManager.bank.AccountBalance}";
                        MessageBox.Show($"This ATM doesnt have enough money to complete your request.\n${Amount.Text} is left.", "ATM doesn't have enough money", MessageBoxButtons.OK);
                        Notification.Text = "Withdrawal unsucessful. Not enough funds in ATM.";
                        labelNoMoney.Visible = true;
                        return;  
                    }
                    if (amount % 10 != 0)
                    {
                        labelWithdrawal.Visible = true;
                        Notification.Text = "Withdrawal unsucessful.";
                        Chequing_CheckedChanged(sender, e);
                        return;

                    }
                    check = save.Withdraw(amount);
                    if (check)
                    {
                        Notification.Text = "Withdrawal sucessful.";
                        Saving_CheckedChanged(sender, e);
                        return;
                    }
                    else
                    {
                        Notification.Text = "Withdrawal unsucessful. Not enough funds.";
                        return;
                    }

                }
            }
            if (Transfer.Checked)
            {
                if (amount>100000)
                {
                    labelTransfer.Visible = true;
                    Notification.Text = "Transfer unsuccessful.";
                    return;
                }
                if (Chequing.Checked)
                {
                    bool check = cheque.TransferOut(amount);
                    if (check)
                    {
                        save.TransferIn(amount);
                        Notification.Text = "Transfer successful.";
                        Chequing_CheckedChanged(sender, e);
                        return;
                    }
                    else
                    {
                        Notification.Text = "Transfer unsuccessful. Not enough funds.";
                        return;
                    }
                }
                if (Saving.Checked)
                {
                    bool check = save.TransferOut(amount);
                    if (check)
                    {
                        cheque.TransferIn(amount);
                        Notification.Text = "Transfer successful.";
                        Saving_CheckedChanged(sender, e);
                        return;
                    }
                    else
                    {
                        Notification.Text = "Transfer unsuccessful. Not enough funds.";
                        return;
                    }
                }
            }
            if (Pay_Bill.Checked)
            {
                if(Chequing.Checked)
                {
                    if (amount>10000)
                    {
                        labelBill.Visible = true;
                        Notification.Text = "Paying bill unsuccessful.";
                    }
                    bool check = cheque.PayBill(amount);
                    if (check)
                    {
                        Notification.Text = "Paying bill successful.";
                        Chequing_CheckedChanged(sender, e);
                        return;
                    }
                    else
                    {
                        Notification.Text = "Paying bill unsuccessful. Not Enough funds.";
                    }
                }
                else
                {
                    labelBillSaving.Visible = true;
                    Notification.Text = "Paying bill unsuccessful.";
                    return;
                }
            }
            
        }

        //
        // Choosing an Account events.
        //

        private void Chequing_CheckedChanged(object sender, EventArgs e)
        {
            if (Chequing.Checked)
            {
                if (cheque == null)
                    Funds.Text = "Chequing account doesn't exist.";
                else
                    Funds.Text = $"${Math.Round(cheque.AccountBalance,2)}";
                return;
            }
            if (!Chequing.Checked && !Saving.Checked)
                Funds.Text = "No account selected.";
        }

        private void Saving_CheckedChanged(object sender, EventArgs e)
        {
            if (Saving.Checked)
            {
                if (save == null)
                    Funds.Text = "Saving account doesn't exist.";
                else
                    Funds.Text = $"${Math.Round(save.AccountBalance,2)}";
                return;
            }
            if (!Chequing.Checked && !Saving.Checked)
                Funds.Text = "No account selected.";
        }

        //
        // Transaction selection events
        //

        private void Deposit_CheckedChanged(object sender, EventArgs e)
        {
            if (Deposit.Checked)
            {
                Notification.Text = "Deposit automatically chooses chequing.";
                Chequing.Checked = true;
                return;
            }
            if (!Transfer.Checked && !Withdrawal.Checked && !Pay_Bill.Checked && !Deposit.Checked)
                Notification.Text = "";
        }

       

        private void Pay_Bill_CheckedChanged(object sender, EventArgs e)
        {
            if (Pay_Bill.Checked)
            {
                Notification.Text = $"You must pay a bil from your chequing account. \nThis transaction will add a flat fee of {ATMClassLibrary.Chequing.BillFee}";
                Chequing.Checked = true;
                return;
            }
            if (!Transfer.Checked && !Withdrawal.Checked && !Pay_Bill.Checked && !Deposit.Checked)
                Notification.Text = "";
        }

        private void Transfer_CheckedChanged(object sender, EventArgs e)
        {
            if (Transfer.Checked)
            {
                Notification.Text = "Choose the account you want to take the money out of.";
                return;
            }
            if(!Transfer.Checked && !Withdrawal.Checked && !Pay_Bill.Checked && !Deposit.Checked)
                Notification.Text = "";
        }

        private void Withdrawal_CheckedChanged(object sender, EventArgs e)
        {
            if (ATMManager.bank.AccountBalance == 0)
                labelNoMoney.Visible = true;
            if (Withdrawal.Checked)
            {
                Notification.Text = "You have to withdraw a multiple of 10. \nChequing accout is the default withdrawal account, with a limit of 1000. \nSaving account doesn't have a limit.";
                return;
            }
            if (!Transfer.Checked && !Withdrawal.Checked && !Pay_Bill.Checked && !Deposit.Checked)
                Notification.Text = "";
        }

       
    }
}
