﻿namespace CDI_ATM
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.Period = new System.Windows.Forms.Button();
            this.Amount = new System.Windows.Forms.TextBox();
            this.Clear = new System.Windows.Forms.Button();
            this.Exit_Button1 = new System.Windows.Forms.Button();
            this.Submit = new System.Windows.Forms.Button();
            this.Transaction = new System.Windows.Forms.GroupBox();
            this.Pay_Bill = new System.Windows.Forms.RadioButton();
            this.Transfer = new System.Windows.Forms.RadioButton();
            this.Withdrawal = new System.Windows.Forms.RadioButton();
            this.Deposit = new System.Windows.Forms.RadioButton();
            this.KeyPad = new System.Windows.Forms.GroupBox();
            this.Account = new System.Windows.Forms.GroupBox();
            this.Saving = new System.Windows.Forms.RadioButton();
            this.Chequing = new System.Windows.Forms.RadioButton();
            this.groupbox2 = new System.Windows.Forms.GroupBox();
            this.Notification = new System.Windows.Forms.TextBox();
            this.labelTransfer = new System.Windows.Forms.Label();
            this.labelBill = new System.Windows.Forms.Label();
            this.labelWithdrawal = new System.Windows.Forms.Label();
            this.labelWithdrawalChequing = new System.Windows.Forms.Label();
            this.groupbox1 = new System.Windows.Forms.GroupBox();
            this.Funds = new System.Windows.Forms.TextBox();
            this.Value = new System.Windows.Forms.Label();
            this.labelAccount = new System.Windows.Forms.Label();
            this.labelTransaction = new System.Windows.Forms.Label();
            this.labelBillSaving = new System.Windows.Forms.Label();
            this.labelNoMoney = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Transaction.SuspendLayout();
            this.KeyPad.SuspendLayout();
            this.Account.SuspendLayout();
            this.groupbox2.SuspendLayout();
            this.groupbox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(30, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(80, 25);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(44, 34);
            this.button2.TabIndex = 1;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(130, 25);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(44, 34);
            this.button3.TabIndex = 2;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(30, 65);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(44, 34);
            this.button4.TabIndex = 3;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(80, 65);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(44, 34);
            this.button5.TabIndex = 4;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(130, 65);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(44, 34);
            this.button6.TabIndex = 5;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(30, 105);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(44, 34);
            this.button7.TabIndex = 6;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(80, 105);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(44, 34);
            this.button8.TabIndex = 7;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(130, 105);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(44, 34);
            this.button9.TabIndex = 8;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(80, 145);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(44, 34);
            this.button0.TabIndex = 9;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // Period
            // 
            this.Period.Location = new System.Drawing.Point(130, 145);
            this.Period.Name = "Period";
            this.Period.Size = new System.Drawing.Size(44, 34);
            this.Period.TabIndex = 10;
            this.Period.Text = ".";
            this.Period.UseVisualStyleBackColor = true;
            this.Period.Click += new System.EventHandler(this.Period_Click);
            // 
            // Amount
            // 
            this.Amount.Location = new System.Drawing.Point(30, 189);
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            this.Amount.Size = new System.Drawing.Size(144, 23);
            this.Amount.TabIndex = 12;
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(445, 326);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(75, 23);
            this.Clear.TabIndex = 13;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Exit
            // 
            this.Exit_Button1.Location = new System.Drawing.Point(527, 325);
            this.Exit_Button1.Name = "Exit";
            this.Exit_Button1.Size = new System.Drawing.Size(75, 23);
            this.Exit_Button1.TabIndex = 14;
            this.Exit_Button1.Text = "Exit";
            this.Exit_Button1.UseVisualStyleBackColor = true;
            this.Exit_Button1.Click += new System.EventHandler(this.Exit_Click);
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(364, 326);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(75, 23);
            this.Submit.TabIndex = 15;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // Transaction
            // 
            this.Transaction.Controls.Add(this.Pay_Bill);
            this.Transaction.Controls.Add(this.Transfer);
            this.Transaction.Controls.Add(this.Withdrawal);
            this.Transaction.Controls.Add(this.Deposit);
            this.Transaction.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Transaction.Location = new System.Drawing.Point(402, 12);
            this.Transaction.Name = "Transaction";
            this.Transaction.Size = new System.Drawing.Size(200, 111);
            this.Transaction.TabIndex = 16;
            this.Transaction.TabStop = false;
            this.Transaction.Text = "Select Transaction:";
            // 
            // Pay_Bill
            // 
            this.Pay_Bill.AutoSize = true;
            this.Pay_Bill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pay_Bill.Location = new System.Drawing.Point(6, 88);
            this.Pay_Bill.Name = "Pay_Bill";
            this.Pay_Bill.Size = new System.Drawing.Size(59, 17);
            this.Pay_Bill.TabIndex = 3;
            this.Pay_Bill.TabStop = true;
            this.Pay_Bill.Text = "Pay Bill";
            this.Pay_Bill.UseVisualStyleBackColor = true;
            this.Pay_Bill.CheckedChanged += new System.EventHandler(this.Pay_Bill_CheckedChanged);
            // 
            // Transfer
            // 
            this.Transfer.AutoSize = true;
            this.Transfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Transfer.Location = new System.Drawing.Point(6, 66);
            this.Transfer.Name = "Transfer";
            this.Transfer.Size = new System.Drawing.Size(96, 17);
            this.Transfer.TabIndex = 2;
            this.Transfer.TabStop = true;
            this.Transfer.Text = "Transfer Funds";
            this.Transfer.UseVisualStyleBackColor = true;
            this.Transfer.CheckedChanged += new System.EventHandler(this.Transfer_CheckedChanged);
            // 
            // Withdrawal
            // 
            this.Withdrawal.AutoSize = true;
            this.Withdrawal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Withdrawal.Location = new System.Drawing.Point(6, 43);
            this.Withdrawal.Name = "Withdrawal";
            this.Withdrawal.Size = new System.Drawing.Size(78, 17);
            this.Withdrawal.TabIndex = 1;
            this.Withdrawal.TabStop = true;
            this.Withdrawal.Text = "Withdrawal";
            this.Withdrawal.UseVisualStyleBackColor = true;
            this.Withdrawal.CheckedChanged += new System.EventHandler(this.Withdrawal_CheckedChanged);
            // 
            // Deposit
            // 
            this.Deposit.AutoSize = true;
            this.Deposit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Deposit.Location = new System.Drawing.Point(6, 20);
            this.Deposit.Name = "Deposit";
            this.Deposit.Size = new System.Drawing.Size(61, 17);
            this.Deposit.TabIndex = 0;
            this.Deposit.TabStop = true;
            this.Deposit.Text = "Deposit";
            this.Deposit.UseVisualStyleBackColor = true;
            this.Deposit.CheckedChanged += new System.EventHandler(this.Deposit_CheckedChanged);
            // 
            // KeyPad
            // 
            this.KeyPad.Controls.Add(this.button5);
            this.KeyPad.Controls.Add(this.button1);
            this.KeyPad.Controls.Add(this.button2);
            this.KeyPad.Controls.Add(this.button3);
            this.KeyPad.Controls.Add(this.button4);
            this.KeyPad.Controls.Add(this.Amount);
            this.KeyPad.Controls.Add(this.button6);
            this.KeyPad.Controls.Add(this.Period);
            this.KeyPad.Controls.Add(this.button7);
            this.KeyPad.Controls.Add(this.button0);
            this.KeyPad.Controls.Add(this.button8);
            this.KeyPad.Controls.Add(this.button9);
            this.KeyPad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPad.Location = new System.Drawing.Point(12, 12);
            this.KeyPad.Name = "KeyPad";
            this.KeyPad.Size = new System.Drawing.Size(200, 223);
            this.KeyPad.TabIndex = 17;
            this.KeyPad.TabStop = false;
            this.KeyPad.Text = "Key Pad";
            // 
            // Account
            // 
            this.Account.Controls.Add(this.Saving);
            this.Account.Controls.Add(this.Chequing);
            this.Account.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Account.Location = new System.Drawing.Point(402, 141);
            this.Account.Name = "Account";
            this.Account.Size = new System.Drawing.Size(200, 70);
            this.Account.TabIndex = 18;
            this.Account.TabStop = false;
            this.Account.Text = "Select Account";
            // 
            // Saving
            // 
            this.Saving.AutoSize = true;
            this.Saving.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Saving.Location = new System.Drawing.Point(7, 44);
            this.Saving.Name = "Saving";
            this.Saving.Size = new System.Drawing.Size(58, 17);
            this.Saving.TabIndex = 1;
            this.Saving.TabStop = true;
            this.Saving.Text = "Saving";
            this.Saving.UseVisualStyleBackColor = true;
            this.Saving.CheckedChanged += new System.EventHandler(this.Saving_CheckedChanged);
            // 
            // Chequing
            // 
            this.Chequing.AutoSize = true;
            this.Chequing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Chequing.Location = new System.Drawing.Point(7, 20);
            this.Chequing.Name = "Chequing";
            this.Chequing.Size = new System.Drawing.Size(70, 17);
            this.Chequing.TabIndex = 0;
            this.Chequing.TabStop = true;
            this.Chequing.Text = "Chequing";
            this.Chequing.UseVisualStyleBackColor = true;
            this.Chequing.CheckedChanged += new System.EventHandler(this.Chequing_CheckedChanged);
            // 
            // groupbox2
            // 
            this.groupbox2.Controls.Add(this.Notification);
            this.groupbox2.Location = new System.Drawing.Point(12, 241);
            this.groupbox2.Name = "groupbox2";
            this.groupbox2.Size = new System.Drawing.Size(200, 65);
            this.groupbox2.TabIndex = 19;
            this.groupbox2.TabStop = false;
            this.groupbox2.Text = "Notification";
            // 
            // Notification
            // 
            this.Notification.Location = new System.Drawing.Point(6, 20);
            this.Notification.Multiline = true;
            this.Notification.Name = "Notification";
            this.Notification.ReadOnly = true;
            this.Notification.Size = new System.Drawing.Size(188, 39);
            this.Notification.TabIndex = 0;
            // 
            // labelTransfer
            // 
            this.labelTransfer.AutoSize = true;
            this.labelTransfer.ForeColor = System.Drawing.Color.Red;
            this.labelTransfer.Location = new System.Drawing.Point(399, 128);
            this.labelTransfer.Name = "labelTransfer";
            this.labelTransfer.Size = new System.Drawing.Size(197, 13);
            this.labelTransfer.TabIndex = 20;
            this.labelTransfer.Text = "No Trasfer can be larger than $100,000.";
            this.labelTransfer.Visible = false;
            // 
            // labelBill
            // 
            this.labelBill.AutoSize = true;
            this.labelBill.ForeColor = System.Drawing.Color.Red;
            this.labelBill.Location = new System.Drawing.Point(300, 128);
            this.labelBill.Name = "labelBill";
            this.labelBill.Size = new System.Drawing.Size(296, 13);
            this.labelBill.TabIndex = 22;
            this.labelBill.Text = "Bills larger than 10,000 must be payed in $10,000 installments";
            this.labelBill.Visible = false;
            // 
            // labelWithdrawal
            // 
            this.labelWithdrawal.AutoSize = true;
            this.labelWithdrawal.ForeColor = System.Drawing.Color.Red;
            this.labelWithdrawal.Location = new System.Drawing.Point(218, 110);
            this.labelWithdrawal.Name = "labelWithdrawal";
            this.labelWithdrawal.Size = new System.Drawing.Size(188, 13);
            this.labelWithdrawal.TabIndex = 23;
            this.labelWithdrawal.Text = "Withdrawals must be a multiple of $10.";
            this.labelWithdrawal.Visible = false;
            // 
            // labelWithdrawalChequing
            // 
            this.labelWithdrawalChequing.AutoSize = true;
            this.labelWithdrawalChequing.ForeColor = System.Drawing.Color.Red;
            this.labelWithdrawalChequing.Location = new System.Drawing.Point(326, 128);
            this.labelWithdrawalChequing.Name = "labelWithdrawalChequing";
            this.labelWithdrawalChequing.Size = new System.Drawing.Size(270, 13);
            this.labelWithdrawalChequing.TabIndex = 24;
            this.labelWithdrawalChequing.Text = "No wthdrawal from chequing can be larger than $1,000.";
            this.labelWithdrawalChequing.Visible = false;
            // 
            // groupbox1
            // 
            this.groupbox1.Controls.Add(this.Funds);
            this.groupbox1.Location = new System.Drawing.Point(402, 229);
            this.groupbox1.Name = "groupbox1";
            this.groupbox1.Size = new System.Drawing.Size(200, 71);
            this.groupbox1.TabIndex = 25;
            this.groupbox1.TabStop = false;
            this.groupbox1.Text = "Funds in sellected account:";
            // 
            // Funds
            // 
            this.Funds.Location = new System.Drawing.Point(6, 20);
            this.Funds.Name = "Funds";
            this.Funds.ReadOnly = true;
            this.Funds.Size = new System.Drawing.Size(188, 20);
            this.Funds.TabIndex = 0;
            this.Funds.Text = "No Account chosen.";
            // 
            // Value
            // 
            this.Value.AutoSize = true;
            this.Value.ForeColor = System.Drawing.Color.Red;
            this.Value.Location = new System.Drawing.Point(213, 174);
            this.Value.Name = "Value";
            this.Value.Size = new System.Drawing.Size(180, 13);
            this.Value.TabIndex = 26;
            this.Value.Text = "Amount must only have one decimal.";
            this.Value.Visible = false;
            // 
            // labelAccount
            // 
            this.labelAccount.AutoSize = true;
            this.labelAccount.ForeColor = System.Drawing.Color.Red;
            this.labelAccount.Location = new System.Drawing.Point(399, 214);
            this.labelAccount.Name = "labelAccount";
            this.labelAccount.Size = new System.Drawing.Size(142, 13);
            this.labelAccount.TabIndex = 27;
            this.labelAccount.Text = "You must select an account.";
            this.labelAccount.Visible = false;
            // 
            // labelTransaction
            // 
            this.labelTransaction.AutoSize = true;
            this.labelTransaction.ForeColor = System.Drawing.Color.Red;
            this.labelTransaction.Location = new System.Drawing.Point(247, 12);
            this.labelTransaction.Name = "labelTransaction";
            this.labelTransaction.Size = new System.Drawing.Size(149, 13);
            this.labelTransaction.TabIndex = 28;
            this.labelTransaction.Text = "You must select a transaction.";
            this.labelTransaction.Visible = false;
            // 
            // labelBillSaving
            // 
            this.labelBillSaving.AutoSize = true;
            this.labelBillSaving.ForeColor = System.Drawing.Color.Red;
            this.labelBillSaving.Location = new System.Drawing.Point(399, 213);
            this.labelBillSaving.Name = "labelBillSaving";
            this.labelBillSaving.Size = new System.Drawing.Size(188, 13);
            this.labelBillSaving.TabIndex = 29;
            this.labelBillSaving.Text = "You must select chequing to pay a bill.";
            this.labelBillSaving.Visible = false;
            // 
            // labelNoMoney
            // 
            this.labelNoMoney.AutoSize = true;
            this.labelNoMoney.ForeColor = System.Drawing.Color.Red;
            this.labelNoMoney.Location = new System.Drawing.Point(15, 325);
            this.labelNoMoney.Name = "labelNoMoney";
            this.labelNoMoney.Size = new System.Drawing.Size(297, 13);
            this.labelNoMoney.TabIndex = 30;
            this.labelNoMoney.Text = "This ATM not longer has money. Comeback later to withdraw.";
            this.labelNoMoney.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(15, 325);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 31;
            this.label1.Visible = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 360);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelNoMoney);
            this.Controls.Add(this.labelBillSaving);
            this.Controls.Add(this.labelWithdrawalChequing);
            this.Controls.Add(this.labelTransaction);
            this.Controls.Add(this.labelAccount);
            this.Controls.Add(this.Value);
            this.Controls.Add(this.groupbox1);
            this.Controls.Add(this.labelWithdrawal);
            this.Controls.Add(this.labelBill);
            this.Controls.Add(this.labelTransfer);
            this.Controls.Add(this.groupbox2);
            this.Controls.Add(this.Account);
            this.Controls.Add(this.KeyPad);
            this.Controls.Add(this.Transaction);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.Exit_Button1);
            this.Controls.Add(this.Clear);
            this.Name = "Main";
            this.Text = "CDI ATM";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Transaction.ResumeLayout(false);
            this.Transaction.PerformLayout();
            this.KeyPad.ResumeLayout(false);
            this.KeyPad.PerformLayout();
            this.Account.ResumeLayout(false);
            this.Account.PerformLayout();
            this.groupbox2.ResumeLayout(false);
            this.groupbox2.PerformLayout();
            this.groupbox1.ResumeLayout(false);
            this.groupbox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button Period;
        private System.Windows.Forms.TextBox Amount;
        private System.Windows.Forms.Button Clear;
#pragma warning disable CS0109 // Member does not hide an inherited member; new keyword is not required
        private new System.Windows.Forms.Button Exit_Button1;
#pragma warning restore CS0109 // Member does not hide an inherited member; new keyword is not required
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.GroupBox Transaction;
        private System.Windows.Forms.RadioButton Pay_Bill;
        private System.Windows.Forms.RadioButton Transfer;
        private System.Windows.Forms.RadioButton Withdrawal;
        private System.Windows.Forms.RadioButton Deposit;
        private System.Windows.Forms.GroupBox KeyPad;
        private System.Windows.Forms.GroupBox Account;
        private System.Windows.Forms.RadioButton Saving;
        private System.Windows.Forms.RadioButton Chequing;
        private System.Windows.Forms.GroupBox groupbox2;
        private System.Windows.Forms.TextBox Notification;
        private System.Windows.Forms.Label labelTransfer;
        private System.Windows.Forms.Label labelBill;
        private System.Windows.Forms.Label labelWithdrawal;
        private System.Windows.Forms.Label labelWithdrawalChequing;
        private System.Windows.Forms.GroupBox groupbox1;
        private System.Windows.Forms.TextBox Funds;
        private System.Windows.Forms.Label Value;
        private System.Windows.Forms.Label labelAccount;
        private System.Windows.Forms.Label labelTransaction;
        private System.Windows.Forms.Label labelBillSaving;
        private System.Windows.Forms.Label labelNoMoney;
        private System.Windows.Forms.Label label1;
    }
}