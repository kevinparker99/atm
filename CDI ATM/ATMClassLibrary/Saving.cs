﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMClassLibrary
{
    public class Saving : Account
    {
        const double interestRate = .01;

        public Saving (string pin, string number, double amount) : base(pin, number, amount) { }

        public static double InterestRate => interestRate;

        public void PayInterest()
        {
            accountBalance += (accountBalance * interestRate);
        }
    }
}
