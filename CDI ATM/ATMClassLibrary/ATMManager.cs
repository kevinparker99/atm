﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ATMClassLibrary
{
    public class ATMManager
    {
        static public DailyBalances dailyBalances = new DailyBalances();
        static public UnicodeEncoding Unicode = new UnicodeEncoding();
        static readonly  chequingAccounts chequingAccount = new chequingAccounts();
        static readonly savingAccounts savingAccount = new savingAccounts();
        static public Bank bank;
        static public DailyBalance dailyBalance;
        static readonly Customers customers = new Customers();


        public void Start()
        {// Reads the three documents into the dictionary.
            char spilt = ',';
            bool check = false;
            
            StreamReader accountReader = new StreamReader("Accounts.txt", Unicode);
            while (!check)
            {
                string line = accountReader.ReadLine();
                if (line == null)
                    break;
                string[] account = line.Split(spilt);
                if (account[0] == "C")
                {
                    chequingAccount.Add(account[1], new Chequing(account[1], account[2], Convert.ToDouble(account[3])));
                }
                if (account[0] == "S")
                {
                    savingAccount.Add(account[1], new Saving(account[1], account[2], Convert.ToDouble(account[3])));
                }
            }
            accountReader.Close();
            StreamReader dailyReader = new StreamReader("DailyBalance.txt", Unicode);
            while (!check)
            {
                string line = dailyReader.ReadLine();
                if (line == null || line == "")
                    break;
                string[] daily = line.Split(spilt);
                try
                {
                    dailyBalances.Add(Convert.ToDateTime(daily[0]), Convert.ToDouble(daily[1]));
                }
                catch(Exception)
                { break; }
                dailyBalance = new DailyBalance(Convert.ToDateTime(daily[0]), Convert.ToDouble(daily[1]));
            }
            dailyReader.Close();
            if (dailyBalance == null)
                dailyBalance = new DailyBalance(DateTime.Today.Date, 5000);
            if (dailyBalance.AtmDate.Date != DateTime.Today.Date)
                dailyBalance.UpdateDailyBalance();
            bank = new Bank("9999", "bank", dailyBalance.AtmBalance);

            StreamReader customerReader = new StreamReader("Customers.txt", Unicode);
            while (!check)
            {
                string line = customerReader.ReadLine();
                if (line == null)
                    break;
                string[] name = line.Split(spilt);
                customers.Add(name[1],name[0]);
                new Customer(name[0], name[1]);
            }
            customerReader.Close();
        }

        public void Closing()
        {// To write the information back into the text file

            StreamWriter customer = new StreamWriter("Customer.txt");
            foreach (KeyValuePair<string, string> i in customers)
            {
                customer.WriteLine($"{i.Value},{i.Key}");
            }
            customer.Close();
            
            StreamWriter account = new StreamWriter("Accounts.txt");
            
            foreach (KeyValuePair<string, Saving> i in savingAccount)
            {
                account.WriteLine($"S,{i.Value.PinNumber},{i.Value.AccountNumber},{i.Value.AccountBalance}");
            }
            foreach (KeyValuePair<string, Chequing> i in chequingAccount)
            {
                account.WriteLine($"C,{i.Value.PinNumber},{i.Value.AccountNumber},{i.Value.AccountBalance}");
            }
            account.Close();
            StreamWriter balance = new StreamWriter("DailyBalance.txt");
            foreach (KeyValuePair<DateTime, double> i in dailyBalances)
            {
                balance.WriteLine($"{i.Key.Date},{i.Value}\n");
            }
            balance.Close();

        }
        
        public int ValidateUser(string name, string pin)
        {// Check if the name and the PIN of the customer matches.
            if (name == null || pin == null)
                return 0;
            bool nameCheck = customers.ContainsValue(name);
            if (!nameCheck)
                return 1;
            bool pinCheck = false;
            try
            {
                if (name == customers[pin])
                    pinCheck = true;
            }
            catch(KeyNotFoundException)
            {
                return 2;
            }
            if (!pinCheck)
                return 2;
            if (!chequingAccount.ContainsKey(pin)&&!savingAccount.ContainsKey(pin))
                return 4;
            return 3;
        }

        public void RemoveCustomer(string name)
        { //Removes the customer for the dictionary if they get their PIN wrong three times.
            customers.Remove(name);
        }

        public (Chequing, Saving) GetAccounts(string PIN)
        { // Loads the chequing and saving account into the Main Form.
            Saving saving = null;
            Chequing chequing = null;
            bool check = savingAccount.ContainsKey(PIN);
            if (check)
            {
                saving = savingAccount[PIN];
            }
            check = chequingAccount.ContainsKey(PIN);
            if (check)
            {
                chequing = chequingAccount[PIN];
            }
            return (chequing, saving);
        }
        public void PayInterest()
        {
            foreach(KeyValuePair<string, Saving> i in savingAccount)
            {
                i.Value.PayInterest();
            }
        }

        public string Print()
        {
            string print = "";
            foreach(KeyValuePair<string, Chequing> i in chequingAccount)
            {
                print += $"{customers[i.Key]}\n";
                print += $"\tChequing:{i.Key}, Account number: {i.Value.AccountNumber}, Balance:{i.Value.AccountBalance}\n";
                if (savingAccount.ContainsKey(i.Key))
                {
                    print += $"\tSaving:{i.Key}, Account number: {savingAccount[i.Key].AccountNumber}, Balance:{savingAccount[i.Key].AccountBalance}\n";
                }
            }
            foreach (KeyValuePair<string, Saving> i in savingAccount)
            {
                
                
                if (!chequingAccount.ContainsKey(i.Key))
                {
                    print += $"{customers[i.Key]}\n";
                    print += $"\tSaving:{i.Key}, Account number: {i.Value.AccountNumber}, Balance:{i.Value.AccountBalance}\n";
                }
            }

            print += $"ATM Balance: {bank.AccountBalance}";
            return print;

        }
    }
}
