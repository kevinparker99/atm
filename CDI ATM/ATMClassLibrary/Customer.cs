﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMClassLibrary
{
    public class Customer
    {
        string pin;
        string name;


        public Customer(string newName, string newPIN)
        {
            name = newName;
            pin = newPIN;
        }

        public string GetPin { get => pin; }
        public string GetName { get => name; }
    }
}
