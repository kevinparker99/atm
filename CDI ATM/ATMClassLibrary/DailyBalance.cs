﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ATMClassLibrary
{
    public class DailyBalance
    {
        DateTime atmDate;
        double atmBalance;
        const int miminumAmount = 0;
        
        public DailyBalance (DateTime date,double i)
        {
            atmBalance = i;
            atmDate = date;
        }
        public double AtmBalance { get => atmBalance;}
        public DateTime AtmDate { get => atmDate;}

        public void UpdateDailyBalance(int amount)
        {
            atmDate = DateTime.Today.Date;
           atmBalance += amount;
            
            ATMManager.dailyBalances.Add(DateTime.Now, atmBalance);
            StreamWriter writer = new StreamWriter("DailyBalance.txt", true);
            writer.Write($"{AtmDate.Date},{atmBalance}\n");
            writer.Close();
            
            
        }

        public void UpdateDailyBalance()
        {
            atmDate = DateTime.Today.Date;
                atmBalance = 5000;
            ATMManager.dailyBalances.Add(DateTime.Now, atmBalance);
            StreamWriter writer = new StreamWriter("DailyBalance.txt", true);
            writer.Write($"{AtmDate.Date},{atmBalance}\n");
            writer.Close();


        }
    }
}
