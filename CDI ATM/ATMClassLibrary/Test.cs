﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ATMClassLibrary
{
    static public class Test
    {
        public static bool AmountTest(string value)
        {
            string[] test = value.Split('.');
            if (test.Length == 1)
                value += ".00";
            if (test.Length > 2)
                return false;
            Regex AmountTest = new Regex(@"^[0-9]{1,}[.]{1}[0-9]{1,2}$");
            return AmountTest.IsMatch(value);
        }

        public static bool NameTest(string name)
        {
            Regex nameTest = new Regex(@"^[a-z A-Z-']{1,30}");
            return nameTest.IsMatch(name);
        }


    }
}
