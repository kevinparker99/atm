﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMClassLibrary
{
    public class Bank : Account
    {
        const double maxTopUp = 20000;
        const double refillAmount = 5000;

        public Bank (string pin, string name, double amount) : base(pin, name, amount) { }

        public static double RefillAmount => refillAmount;

        public static double MaxTopUp => maxTopUp;

        public bool RefillATM(double amount)
        {
            if (amount % refillAmount != 0 || ( amount + accountBalance ) > maxTopUp)
                return false;
            accountBalance += amount;
            return true;
        }
    }
}
