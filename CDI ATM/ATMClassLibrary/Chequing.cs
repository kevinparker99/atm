﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMClassLibrary
{
    public class Chequing: Account
    {
        const double maximumWithDrawal = 1000;
        const double maximumBillAmount = 10000;
        const double billFee = 1.25;
        public Chequing(string pin, string number, double amount) : base(pin, number, amount) { }

        public static double BillFee => billFee;

        public bool PayBill (double amount)
        {
            if ((amount + billFee) > AccountBalance || amount > maximumBillAmount)
                return false;
            accountBalance -= (amount + billFee);
            return true;
        }
        public override bool Withdraw(double amount)
        {
            if (amount > maximumWithDrawal)
                return false;
            return base.Withdraw(amount);
        }
    }
}
