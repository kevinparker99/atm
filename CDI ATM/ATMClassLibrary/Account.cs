﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMClassLibrary
{
    public class Account
    {
        string pinNumber;
        string accountNumber;
        protected double accountBalance;
        const double maximumTransferAmount = 100000;
        public string PinNumber { get => pinNumber;}
        public string AccountNumber { get => accountNumber; }
        public double AccountBalance { get => accountBalance;}

        public Account (string pin, string number, double amount)
        {
            pinNumber = pin;
            accountNumber = number;
            accountBalance = amount;
        }

        public virtual bool Withdraw(double amount)
        {
            if (amount > accountBalance)
                return false;
            accountBalance -= amount;
            return true;
        }

        public void Deposit (double amount)
        {
            accountBalance += amount;
        }

        public bool TransferOut (double amount)
        {
            if (amount > accountBalance)
                return false;
            accountBalance -= amount;
            return true;
        }
        

        public void TransferIn (double amount)
        {
            accountBalance += amount;
        }
    }
}
